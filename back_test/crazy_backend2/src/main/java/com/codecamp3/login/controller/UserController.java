package com.codecamp3.login.controller;

import com.codecamp3.login.domain.BankAccount;
import com.codecamp3.login.domain.RegistaionRequest;
import com.codecamp3.login.domain.User;
import com.codecamp3.login.service.UserService;
import com.codecamp3.login.service.util.JsonService;
import com.codecamp3.login.service.util.PasswordUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static java.util.Objects.isNull;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;
    JsonService jsonService = new JsonService();

    @PostMapping("/login")
    public ResponseEntity<User> login(@RequestBody String jsonUserObject) {

        System.out.println("login is working...");


        User user = jsonService.parseJsonToUser(jsonUserObject);

        String username = user.getUsername();
        User userFromDb = userService.findByUsername(username);


        PasswordUtil passwordUtil = new PasswordUtil();
        boolean isLoginSuccess = passwordUtil.matches(user.getPassword(), userFromDb.getPassword());


        if (isLoginSuccess == true) {
            // login success
            // should send userFromDb to hide password
            return new ResponseEntity<>(userFromDb, HttpStatus.OK);
        } else {
            // TODO change status
            return new ResponseEntity<>(userFromDb, HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping(path = "/register")
    public ResponseEntity<String> registation(@RequestBody String jsonUserObject) {
        RegistaionRequest registaionRequest = jsonService.parseJsonToRegistaionRequest(jsonUserObject);

        //validate server side
        boolean validate = userService.registerAndValidation(registaionRequest.getUsername(), registaionRequest.getEmail(), registaionRequest.getPassword(), registaionRequest.getConfirmPassword());
        System.out.println("val************************** : "+validate);
        User user = new User();
        if (validate == true) {
            String passEncrypted = new PasswordUtil().encryptPassword(registaionRequest.getPassword());

            //set started bank
            String date = "recently";

            BankAccount bankAccount = new BankAccount(registaionRequest.getUsername(),date, BigDecimal.valueOf(100.00));

            user = userService.registration(registaionRequest.getUsername(), registaionRequest.getEmail(), registaionRequest.getFullname(), passEncrypted,registaionRequest.getPin(),bankAccount);
            return new ResponseEntity<String>("success", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("fail", HttpStatus.CONFLICT);
        }
    }

}
