package com.codecamp3.login.service.util;

import com.codecamp3.login.domain.BankAccount;
import com.codecamp3.login.domain.BankRequest;
import com.codecamp3.login.domain.RegistaionRequest;
import com.codecamp3.login.domain.User;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JsonService {

  public User parseJsonToUser(String jsonUserObject){
    User user = null;

    try {
      ObjectMapper mapper = new ObjectMapper();
      user = mapper.readValue(jsonUserObject, User.class);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return user;
  }

  public RegistaionRequest parseJsonToRegistaionRequest(String jsonUserObject){
    RegistaionRequest userRegis = null;

    try {
      ObjectMapper mapper = new ObjectMapper();
      userRegis = mapper.readValue(jsonUserObject, RegistaionRequest.class);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return userRegis;
  }

  public BankRequest parseJsonToBankRequest(String jsonUserObject){
    BankRequest bank = null;

    try {
      ObjectMapper mapper = new ObjectMapper();
      bank = mapper.readValue(jsonUserObject, BankRequest.class);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return bank;
  }
}
