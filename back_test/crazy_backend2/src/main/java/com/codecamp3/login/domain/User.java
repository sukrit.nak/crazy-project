package com.codecamp3.login.domain;


import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {


    @Id
    private String username;
    private String email;
    @Column(name = "full_name")
    private String fullName;
    private String password;
    private Integer pin;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "username", insertable = false, updatable = false)
    private BankAccount bankAccount;

    public User() {
    }


    public User(String username, String email, String fullName, String password, Integer pin, BankAccount bankAccount) {
        this.username = username;
        this.email = email;
        this.fullName = fullName;
        this.password = password;
        this.pin = pin;
        this.bankAccount = bankAccount;
    }

    public Integer getPin() {
        return pin;
    }

    public void setPin(Integer pin) {
        this.pin = pin;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", fullName='" + fullName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
