package com.codecamp3.login.service;

import com.codecamp3.login.domain.User;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class BankService {

    public static Boolean enough(User person,BigDecimal amount) {
        BigDecimal balance = person.getBankAccount().getBalance();

        int res = balance.compareTo(amount);

        if (res == 0) {
            System.out.println("Both values are equal");
            return true;
        } else if (res == 1) {
            System.out.println("First Value is greater");
            return true;
        } else if (res == -1) {
            System.out.println("Second value is greater");
            return false;
        }
        return false;

    }

    public static User withdraw(User person, BigDecimal amount) {
        BigDecimal balance = person.getBankAccount().getBalance();
        BigDecimal newBalance = balance.subtract(amount);
        person.getBankAccount().setBalance(newBalance);
        return person;
    }

    public static User deposit(User person, BigDecimal amount,String date) {
        BigDecimal balance = person.getBankAccount().getBalance();
        BigDecimal newBalance = balance.add(amount);
        person.getBankAccount().setBalance(newBalance);
        person.getBankAccount().setDate(date);
        return person;
    }
}
