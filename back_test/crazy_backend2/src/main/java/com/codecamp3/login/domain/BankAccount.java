package com.codecamp3.login.domain;


import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "bank_account")
public class BankAccount {

    @Id
    private String username;
    private String date;
    private BigDecimal balance;

    public BankAccount() {
    }

    public BankAccount(String username, String date, BigDecimal balance) {
        this.username = username;
        this.date = date;
        this.balance = balance;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "username='" + username + '\'' +
                ", date='" + date + '\'' +
                ", balance=" + balance +
                '}';
    }
}
