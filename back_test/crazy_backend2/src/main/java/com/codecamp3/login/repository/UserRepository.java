package com.codecamp3.login.repository;

import com.codecamp3.login.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
  // login case
  User findByUsernameAndPassword(String username, String password);

  //register case
  User findByUsername(String username);
  User findByEmail(String email);

  //bank case
  User findByUsernameAndPin(String username,Integer pin);

}
