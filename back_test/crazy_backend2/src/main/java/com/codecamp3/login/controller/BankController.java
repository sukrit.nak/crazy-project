package com.codecamp3.login.controller;

import com.codecamp3.login.domain.BankAccount;
import com.codecamp3.login.domain.BankRequest;
import com.codecamp3.login.domain.User;
import com.codecamp3.login.repository.BankAccountRepository;
import com.codecamp3.login.repository.UserRepository;
import com.codecamp3.login.service.BankService;
import com.codecamp3.login.service.util.JsonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@CrossOrigin
public class BankController {

    @Autowired
    UserRepository userRepository;
    JsonService jsonService = new JsonService();

    @PostMapping(path = "/witdraw")
    public ResponseEntity<String> witdraw(@RequestBody String jsonUserObject) {
        BankRequest bank = jsonService.parseJsonToBankRequest(jsonUserObject);
        String useBal = bank.getBalance().toString();

        User id2 = userRepository.findByUsernameAndPin(bank.getUsername(), bank.getPin());

        Boolean check = BankService.enough(id2, bank.getBalance());
        BigDecimal balanceNotEnough = id2.getBankAccount().getBalance();
        String checkNotEnough = balanceNotEnough.toString();

        if (check == true) {

            User person = BankService.withdraw(id2, bank.getBalance());
            BigDecimal balance = person.getBankAccount().getBalance();
            String checkBal = balance.toString();
            userRepository.save(person);
            return new ResponseEntity<String>("You pay your coin in " + useBal + ", your coin is :" + checkBal, HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("You only have : " + checkNotEnough + ",if you pay your coin in full (" + useBal + ") you’ll have a negative balance. ", HttpStatus.OK);
        }
    }

    @PostMapping(path = "/deposit")
    public ResponseEntity<String> deposit(@RequestBody String jsonUserObject) {
        BankRequest bank = jsonService.parseJsonToBankRequest(jsonUserObject);
        String getCoin = bank.getBalance().toString();

        User id = userRepository.findByUsername(bank.getUsername());
        BigDecimal balanceBefore = id.getBankAccount().getBalance();
        String balance_present = balanceBefore.toString();

        Boolean checkDate = id.getBankAccount().getDate().equals(bank.getDate());
        if (checkDate == false) {

            User person = BankService.deposit(id, bank.getBalance(), bank.getDate());
            BigDecimal balance = person.getBankAccount().getBalance();
            String checkBal = balance.toString();
            userRepository.save(person);

            return new ResponseEntity<String>("You get coin in " + getCoin + ", your coin is :" + checkBal, HttpStatus.OK);
        } else
            return new ResponseEntity<String>("You might want to repeat a game to check your destiny, but you cannot get a coin. Your coin is :" + balance_present, HttpStatus.OK);
    }

    @PostMapping(path = "check/coin")
    public ResponseEntity<String> check(@RequestBody String jsonUserObject) {
        BankRequest bank = jsonService.parseJsonToBankRequest(jsonUserObject);

        User id = userRepository.findByUsername(bank.getUsername());
        BigDecimal balanceBefore = id.getBankAccount().getBalance();
        String balance_present = balanceBefore.toString();

        return new ResponseEntity<String>("Your coin is :" + balance_present, HttpStatus.OK);
    }
}
