package com.codecamp3.login.service;

import com.codecamp3.login.domain.BankAccount;
import com.codecamp3.login.domain.User;
import com.codecamp3.login.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public User login(String username, String password) {

        User user = userRepository.findByUsernameAndPassword(username, password);
        return user;
    }

    public User registration(String username, String email, String fullname, String password,Integer pin, BankAccount bankAccount) {
        User user = new User(username, email, fullname, password,pin, bankAccount);
        System.out.println(user);

        return userRepository.save(user);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    // validate in server side

    public boolean registerAndValidation(String username, String email, String password, String confirmPassword) {


        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        Boolean check = pat.matcher(email).matches();

        if (check == true) {

            User emailDb = userRepository.findByEmail(email);
            User usernameDb = userRepository.findByUsername(username);


            System.out.println(emailDb);
            System.out.println(usernameDb);
            if (emailDb == null && password.equals(confirmPassword) && usernameDb == null) {
                return true;
            } else {
                return false;
            }
        } else {
            System.out.println("It's not email ************************");
            return false;
        }
    }

}
